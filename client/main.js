import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.editor.helpers({
	docid:function(){
		//console.log("doc id finlter");
		console.log(Documents.findOne());
		var doc=Documents.findOne();
		if(doc){
			return Documents.findOne()._id;
		}else{
			return undefined;
		}
		
	},//recibimos al editor, ahora lo podemos configurar
	config:function(){
		return function(editor){//cm_editor es una instancia del editor, e info guarda toda la info sobre los cambios(from,to,text,removed,origin)
			//configurando el tema cobal
			editor.setOption("LineNumbers",true);
			editor.setOption("theme","cobalt");
			editor.on("change",function(cm_editor, info){
				console.log(cm_editor.getValue());
				$("#viewer_iframe").contents().find("html").html(cm_editor.getValue());
				Meteor.call("addEditingUser");
			});
			//console.log(editor);
		}
	},
});

Template.editingUsers.helpers({
	users:function(){
		var doc,eusers,users;
		doc= Documents.findOne();
		if(!doc){return;}//give up
		eusers=EditingUsers.findOne({docid:doc._id});
		if(!eusers){return;}//give up
		//ahora iteraremos las key del objeto
		users=new Array();
		var i=0;
		for(var user_id in eusers.users){
			users[i]=fixObjectKeys(eusers.users[user_id]);
			i++;
		}

		return users;
	}
});


function fixObjectKeys(obj){
	var newObj={};
	for (key in obj){
		var key2=key.replace("-","");
		newObj[key2]=obj[key];
	}
	return newObj;

}

/*
var myVar=10;

//ahora crearemos una session reactive

//actualizara el current_data cada 1 seg

Meteor.setInterval(function(){

	Session.set("current_date",new Date());

},1000);


Template.date_display.helpers({
	current_date: function(){
		return Session.get("current_date");
	},
	myVar:function(){
		return myVar;
	}
});

*/


