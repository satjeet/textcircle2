import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
  if(!Documents.findOne()){ //continua si no encuentra uno
  	Documents.insert({title:"my nuevo documentos"});
  }
});


Meteor.methods({
	addEditingUser:function(){
		var doc,user,eusers;
		doc=Documents.findOne();
		if(!doc){return;}// sin documento, rendirse
		if(!this.userId){return;}// no logged-in user, rendirse
		//tengo user y el doc
		user=Meteor.user().profile;
		eusers=EditingUsers.findOne({docid:doc._id});
		if(!eusers){
			eusers={
				docid:doc._id,
				users:{},
			};
		}

		user.lastEdit=new Date();
		//Meteor.userId o this.userId me entrega la id del usuario
		eusers.users[this.userId]=user;

		EditingUsers.upsert({_id:eusers._id},eusers);
	}
})
